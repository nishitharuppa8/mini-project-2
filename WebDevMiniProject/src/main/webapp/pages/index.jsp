<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page session="true" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CUSTOMER MANAGEMENTt</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>



<div style="color:black;background: green;height: 100px"class="topic"><center>
<br>
 
 <h1 style="font-size:3vw">CUSTOMER RELATIONSHIP MANAGEMENT </h1> </center></div>

  <a href="addCustomer"><button>Add Customer</button></a>
  
</div>

<div class="modal-content animate">
		<table border="1" width="70%" cellpadding="2" >
		
			<thead class = "table-dark">
				<tr>
				
					<th><b>FIRST NAME</b></th>
					<th><b>LAST NAME</b></th>
					<th><b>EMAIL</b></th>
					<th colspan="2"><b>ACTION</b></th>
					
				</tr>
			</thead>


	<c:forEach var ="i" items="${customer}" >		
				<tr align="center">
					
					<td><c:out value = "${i.getFirstName() }"></c:out></td>
					<td><c:out value = "${i.getLastName() }"></c:out></td>
					<td><c:out value = "${i.getEmail() }"></c:out></td>
				 	<td colspan="2"><a href = "delete/${i.getId()}"><button class="btn btn-danger" >Delete</button></a>
					<a href="edit/${i.getId()}"><button class="btn btn-primary">Update</button></a></td>
					
				</tr>
		</c:forEach>

		</table>
		<p><%=session.getAttribute("message")%></p>
	</div>

</body>
</html>