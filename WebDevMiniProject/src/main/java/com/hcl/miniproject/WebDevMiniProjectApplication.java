package com.hcl.miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.hcl.miniproject","com.hcl.miniproject.controller","com.hcl.miniproject.bean","com.hcl.miniproject.service","com.hcl.miniproject.repository"})
public class WebDevMiniProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebDevMiniProjectApplication.class, args);
	}

}
