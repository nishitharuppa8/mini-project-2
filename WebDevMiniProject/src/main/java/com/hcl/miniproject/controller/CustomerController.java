package com.hcl.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.hcl.miniproject.bean.Customer;
import com.hcl.miniproject.service.CustomerService;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping("/")
	public String viewHomePage(Model model) {
		List<Customer> customer=customerService.getAllCustomer();
		model.addAttribute("customer",customer);
		return "index";
	}

	@GetMapping("/addCustomer")
	public String addCustomer() {
		return "addCustomer";
	}

	@PostMapping("/register")
	public String customerRegister(@ModelAttribute Customer customer,HttpSession httpSession) {
		System.out.println(customer);

		customerService.addCustomer(customer);
			httpSession.setAttribute("message", "Customer is Added ");

		return "redirect:/";

	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable int id,Model model) {
		Customer customer=customerService.getCustomerById(id);
		model.addAttribute("customer", customer);
		return "update";
	}

	@PostMapping("/update")
	public String updateCustomer(@ModelAttribute Customer customer,HttpSession httpSession) {
		customerService.addCustomer(customer);
		System.out.println(customer);
		httpSession.setAttribute("message", "Customer Data is Updated ");
		return "redirect:/";

	}

	@GetMapping("/delete/{id}")
	public String deleteCustomer(@PathVariable int id,HttpSession httpSession) {

		System.out.println("Deleted Id = "+id );
		customerService.deleteCustomer(id);
		httpSession.setAttribute("message", "Customer Data is Deleted");
		return "redirect:/";
	}

}
