package com.hcl.miniproject.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.miniproject.bean.Customer;
import com.hcl.miniproject.repository.CustomerRepository;

@Service	
public class CustomerService {
	
	@Autowired
	public CustomerRepository customerRepo;
	
	public void addCustomer(Customer customer) {
		customerRepo.save(customer);
	}
	
	public List<Customer> getAllCustomer(){
		return customerRepo.findAll();
	}
	
	public Customer getCustomerById(int id) {
		Optional<Customer> customer=customerRepo.findById(id); 
	if(customer.isPresent()) {
		return customer.get();
	}
	return null;
	
	}
	 
	
	public void deleteCustomer(int id) {
		System.out.println("Deleted Customer Id = "+id);
		customerRepo.deleteById(id);
	}
}
